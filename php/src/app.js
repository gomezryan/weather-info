function submitForm() {
    var city = document.getElementById("city").value;
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=eaae16fa34404fbfa89e4b0429d37ff4`)
    .then(response => response.json())
    .then(data => {
        var temperature = data.main.temp;
        var weather = data.weather[0].main;
        var description = data.weather[0].description;
        saveData(temperature, weather, description,city);
    })
    .catch(error => console.error(error));
}

function saveData(temperature, weather, description,city) {
    var data = JSON.stringify({city: city, temperature: temperature, weather: weather, description: description});
    fetch('save-data.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: data
    })
    .then(response => response.text())
    .then(result => {
        var resultElement = document.getElementById("result");
        resultElement.innerHTML = result;
    })
    .catch(error => console.error(error));
}
