<!DOCTYPE html>
<html>
  <head>
    <title>Weather App</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Weather App</h1>
    <form id="form" method="POST" action="javascript:submitForm()">
      <label for="city">Enter the name of the city:</label>
      <input type="text" id="city" name="city">
      <input id="submit" type="submit" value="Submit" onclick="submitForm()">
    </form>
    <h1>Weather info</h1>
    <div id="weather-info"></div>
    <h1>Map</h1>
    <div id="map"><iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-16.325683593750004%2C39.791654835253425%2C18.786621093750004%2C52.70967533219885&amp;layer=mapnik&amp;marker=46.63435070293566%2C1.23046875" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=46.634&amp;mlon=1.230#map=6/46.634/1.230">View Larger Map</a></small></div>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="script.js"></script>
    <script src="app.js"></script>
  </body>
</html>
