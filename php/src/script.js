let map = L.map("map").setView([0, 0], 2);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
let marker = L.marker([0, 0]).addTo(map);

document.getElementById("submit").addEventListener("click", function(event) {
    event.preventDefault();
    let city = document.getElementById("city").value;
    let weatherInfo = document.getElementById("weather-info");
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=eaae16fa34404fbfa89e4b0429d37ff4`)
        .then(response => response.json())
        .then(data => {
            if (data.cod === "404") {
                weatherInfo.innerHTML = `<p class='error'>${data.message}</p>`;
                return;
            }
            let temperature = data.main.temp;
            let weather = data.weather[0].main;
            let weatherDescription = data.weather[0].description;
            let lat = data.coord.lat;
            let lng = data.coord.lon;
            temperature = (temperature - 273.15).toFixed(2);
            weatherInfo.innerHTML = `
                <p>Temperature: ${temperature} &#8451;</p>
                <p>Weather: ${weather}</p>
                <p>Description: ${weatherDescription}</p>
            `;
            map.setView([lat, lng], 13);
            marker.setLatLng([lat, lng]);
            marker.bindPopup("<b>City:</b> " + city + "<br>" + "<b>Temperature:</b> " + temperature + " &#8451;<br>" +            "<b>Weather:</b> " + weather + "<br>" + "<b>Description:</b> " + weatherDescription).openPopup();
            map.on('click', onMapClick);

            function onMapClick(e) {
                marker.setLatLng(e.latlng);
                marker.update();
                fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${e.latlng.lat}&lon=${e.latlng.lng}&appid=eaae16fa34404fbfa89e4b0429d37ff4`)
                    .then(response => response.json())
                    .then(data => {
                        if (data.cod === "404") {
                            weatherInfo.innerHTML = `<p class='error'>${data.message}</p>`;
                            return;
                        }
                        let temperature = data.main.temp;
                        let weather = data.weather[0].main;
                        let weatherDescription = data.weather[0].description;
                        temperature = (temperature - 273.15).toFixed(2);
                        marker.bindPopup("<b>City:</b> " + data.name + "<br>" + "<b>Temperature:</b> " + temperature + " &#8 451;<br>" + "<b>Weather:</b> " + weather + "<br>" + "<b>Description:</b> " + weatherDescription).openPopup();
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        })
        .catch(error => {
            console.log(error);
        });
        
        
});



