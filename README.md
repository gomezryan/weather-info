Application "Weather Info"

J'ai mis en place un site fait avec PHP, Javascript et CSS donnant à l'instant T des informations météos concernant n'importe quelle ville du monde.
L'utilisateur n'a qu'à entrer le nom bien orthographié de la ville et par API les résultats sont donnés.
Egalement, ces résultats sont stockés dans une base de données MYSQL dont les identifiants sont : 
username  = "MYSQL_USER"
password = "MYSQL_PASSWORD"

Afin d'accéder à la page, après avoir lancé la commande 'docker-compose up', il faut se rendre sur le navigateur à l'adresse "localhost:8000/index.php". Pour visualiser la base de données, un client PHPmyAdmin est disponible au port 8080 (localhost:8080). Les identifiants sont ceux donnés ci dessous, la base de données étant "MYSQL_DATABASE" et la table "weather". Petite précision, les températures sont stockés en kelvin, donc non éronnées :) 

Trois services sont présents dans le docker compose : phpapache, mysql et phpmyadmin.